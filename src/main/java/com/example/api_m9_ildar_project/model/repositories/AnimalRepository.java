package com.example.api_m9_ildar_project.model.repositories;

import com.example.api_m9_ildar_project.model.entities.Animal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


@Repository
public interface  AnimalRepository extends JpaRepository <Animal,Integer>{

    List<Animal> findAnimalByAlimentation(String alimentation);
    List<Animal> findAnimalByOrder(String order);
    List<Animal> findAnimalByFamily(String family);
    List<Animal> findAnimalByClasification(String clasification);
    Animal findAnimalByName(String name);


}
