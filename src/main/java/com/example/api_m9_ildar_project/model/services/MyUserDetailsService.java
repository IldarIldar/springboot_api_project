package com.example.api_m9_ildar_project.model.services;


        import lombok.RequiredArgsConstructor;
        import org.springframework.security.core.userdetails.UserDetails;
        import org.springframework.security.core.userdetails.UserDetailsService;
        import org.springframework.security.core.userdetails.UsernameNotFoundException;
        import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MyUserDetailsService implements UserDetailsService {

    private final UserService serveiUsuarisUserDetails;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return serveiUsuarisUserDetails.consultByUsername(username);
    }

    public UserDetails loadUserById(Long id){
        return serveiUsuarisUserDetails.consultarPerId(id);
    }
}