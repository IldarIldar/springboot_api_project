package com.example.api_m9_ildar_project.model.services;

import com.example.api_m9_ildar_project.model.entities.Animal;
import com.example.api_m9_ildar_project.model.repositories.AnimalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AnimalService {

    private final AnimalRepository animalRepo;

    //POST
    public Animal postAnimal(Animal animal){
        return animalRepo.save(animal);
    }

    //GET
    public List<Animal> findAllAnimals() {
        return animalRepo.findAll();
    }

    public List<Animal> findAnimalByOrder(String order) {
        return animalRepo.findAnimalByOrder(order);}

    public List<Animal> findAnimalByFamily(String family) {
        return animalRepo.findAnimalByFamily(family);
    }

    public List<Animal> findAnimalByClasification(String clasification) {
        return animalRepo.findAnimalByClasification(clasification);
    }

    public Animal findAnimalByName(String name) {
        return animalRepo.findAnimalByName(name);
    }

    //PUT
//    public Animal putAnimal(Animal animal){
//        return animalRepo.
//    }

    //DELETES
    public ResponseEntity<?> deleteAnimal(int id_animal){
      //first we search for an animal with the same id
      Optional<Animal> animal=  findAllAnimals().stream().filter(an->an.getIdItem()==id_animal).findFirst();

        //if that animal exist we delete him
        if (!animal.isEmpty()) {
            animalRepo.delete(animal.get());
            return ResponseEntity.noContent().build();
        } else  return ResponseEntity.notFound().build();
    }

}
