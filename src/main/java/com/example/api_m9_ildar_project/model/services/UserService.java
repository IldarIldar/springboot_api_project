package com.example.api_m9_ildar_project.model.services;


import com.example.api_m9_ildar_project.model.entities.User;
import com.example.api_m9_ildar_project.model.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
        import org.springframework.security.crypto.password.PasswordEncoder;
        import org.springframework.stereotype.Service;

        import java.util.ArrayList;
        import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepo;

    private final PasswordEncoder cyphred;

    public User consultByUsername(String username) {
        return userRepo.findByUsername(username).orElse(null);
    }

    public User createNewUsername(User newUser) {
        //falta controlar que els 2 passwords del client coincideixen
        //passar un UserCreacioDTO
        newUser.setPassword(cyphred.encode(newUser.getPassword()));
        userRepo.save(newUser);
        return newUser;
    }
    public User consultarPerId(Long id){
        return userRepo.findById(id).orElse(null);
    }
    public List<User> listUsers(){
        return userRepo.findAll();
    }


}