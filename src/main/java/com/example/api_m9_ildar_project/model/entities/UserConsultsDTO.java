package com.example.api_m9_ildar_project.model.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserConsultsDTO {
    private String username;
}