package com.example.api_m9_ildar_project.model.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
public class Animal {
    @Id
    @GeneratedValue
    private int idItem;
    private String name;
    private String clasification;
    private String order;
    private String family;
    private String alimentation;
    private int liveLength;


}
