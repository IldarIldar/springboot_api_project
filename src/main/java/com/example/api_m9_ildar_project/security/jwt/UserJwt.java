package com.example.api_m9_ildar_project.security.jwt;

import com.example.api_m9_ildar_project.model.entities.UserConsultsDTO;
import lombok.Builder;
        import lombok.Getter;
        import lombok.NoArgsConstructor;
        import lombok.Setter;

//@NoArgsConstructor
@Getter
@Setter
public class UserJwt extends UserConsultsDTO {
    private String token;

    @Builder(builderMethodName = "jwtUsuariJwtBuilder")
    public UserJwt(String username, String token) {
        super(username);
        this.token = token;
    }
}