package com.example.api_m9_ildar_project.security;
import com.example.api_m9_ildar_project.model.services.MyUserDetailsService;
import com.example.api_m9_ildar_project.security.jwt.JwtAuthorizationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class ConfigurationWebSecurity extends WebSecurityConfigurerAdapter {

        //private final MyConfigurationEntryPoint myConfigurationEntryPoint;
        private final MyUserDetailsService myUserDetailsService;
        private final PasswordEncoder cypher;
        private final JwtAuthorizationFilter jwtAuthorizationFilter;

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(myUserDetailsService).passwordEncoder(cypher);
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .httpBasic()
                    .and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    //per poder accedir al h2-console
                    //  .authorizeRequests().antMatchers("/").permitAll().and()
                    //  .authorizeRequests().antMatchers("/h2-console/**").permitAll()
                    // .and()
                    .csrf().disable()
                    // .headers().frameOptions().disable()
                    // .and()
                    .authorizeRequests()
                    .anyRequest().authenticated().and()
                    .addFilterBefore(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class);
        }


}
