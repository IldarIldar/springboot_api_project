package com.example.api_m9_ildar_project.controller;



import com.example.api_m9_ildar_project.model.entities.User;
import com.example.api_m9_ildar_project.model.entities.UserConsultsDTO;
import com.example.api_m9_ildar_project.model.services.UserService;
import com.example.api_m9_ildar_project.security.jwt.JwtProvider;
import com.example.api_m9_ildar_project.security.jwt.LoginPassword;
import com.example.api_m9_ildar_project.security.jwt.UserJwt;
import lombok.RequiredArgsConstructor;
        import org.springframework.dao.DataIntegrityViolationException;
        import org.springframework.http.HttpStatus;
        import org.springframework.http.ResponseEntity;
        import org.springframework.security.access.prepost.PreAuthorize;
        import org.springframework.security.authentication.AuthenticationManager;
        import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
        import org.springframework.security.core.Authentication;
        import org.springframework.security.core.annotation.AuthenticationPrincipal;
        import org.springframework.security.core.context.SecurityContextHolder;
        import org.springframework.web.bind.annotation.*;
        import org.springframework.web.server.ResponseStatusException;

        import java.util.ArrayList;
        import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController {
    private final UserService serveiUsuaris;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider tokenProvider;

    @PostMapping("/login")
    public ResponseEntity<UserJwt> login(@RequestBody LoginPassword userPassword)
    {
        Authentication auth=authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userPassword.getUsername(),userPassword.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(auth);
        User usu=(User)auth.getPrincipal();
        String jwtToken = tokenProvider.generateToken(auth);
        UserJwt usu2=new UserJwt(usu.getUsername(),jwtToken);
        //es retorna userName, Avatar, Rol i Token
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(usu2);
    }


    @GetMapping("/login")
    public UserConsultsDTO login(@AuthenticationPrincipal User usu){
        UserConsultsDTO usu2=new UserConsultsDTO(usu.getUsername());
        return usu2;
    }



}