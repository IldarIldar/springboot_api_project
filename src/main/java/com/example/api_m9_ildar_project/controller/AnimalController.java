package com.example.api_m9_ildar_project.controller;


import com.example.api_m9_ildar_project.model.entities.Animal;
import com.example.api_m9_ildar_project.model.services.AnimalService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class AnimalController {

    private final AnimalService animalService;


    //POST
    @PostMapping("/animals")
    public ResponseEntity<?> novaTodoList(@RequestBody Animal animal) {
        Animal res = animalService.postAnimal(animal);
        return new ResponseEntity<Animal>(res, HttpStatus.CREATED);
    }


    //PUT



    //GET
    @GetMapping("/animals/")
    public ResponseEntity<?> findAllAnimals() {
        if(animalService.findAllAnimals()==null){
            return ResponseEntity.notFound().build();
        }
        else {
            List<Animal> res = animalService.findAllAnimals();
            res.forEach(System.out::println);

            return ResponseEntity.ok(res);
        }
    }

    @GetMapping("/animals/")
    public ResponseEntity<?> findAllAnimalsByOrder(@RequestBody String order) {
        if(animalService.findAnimalByOrder(order)==null){
            return ResponseEntity.notFound().build();
        }
        else {
            List<Animal> res = animalService.findAnimalByOrder(order);
            res.forEach(System.out::println);

            return ResponseEntity.ok(res);
        }
    }

    @GetMapping("/animals/")
    public ResponseEntity<?> findAllAnimalsByClass(@RequestBody String classification) {
        if(animalService.findAnimalByClasification(classification)==null){
            return ResponseEntity.notFound().build();
        }
        else {
            List<Animal> res = animalService.findAnimalByClasification(classification);
            res.forEach(System.out::println);

            return ResponseEntity.ok(res);
        }
    }

    @GetMapping("/animals/")
    public ResponseEntity<?> findAllAnimalsByFamily(@RequestBody String fam) {
        if(animalService.findAnimalByFamily(fam)==null){
            return ResponseEntity.notFound().build();
        }
        else {
            List<Animal> res = animalService.findAnimalByFamily(fam);
            res.forEach(System.out::println);

            return ResponseEntity.ok(res);
        }
    }


    @GetMapping("/animals/")
    public ResponseEntity<?> findAllAnimalsByName(@RequestBody String name) {
        if(animalService.findAnimalByName(name)==null){
            return ResponseEntity.notFound().build();
        }
        else {
            Animal res = animalService.findAnimalByName(name);
            res.toString();
            return ResponseEntity.ok(res);
        }
    }




    //DELETE
    @DeleteMapping("/todolists/{id_animal}")
    public ResponseEntity<?> eliminarUnAnimal(@PathVariable int id_animal) {

      ResponseEntity<?> re= animalService.deleteAnimal(id_animal);

      return re;
    }

}
