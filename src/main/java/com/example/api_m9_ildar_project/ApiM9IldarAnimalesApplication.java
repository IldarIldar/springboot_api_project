package com.example.api_m9_ildar_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiM9IldarAnimalesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiM9IldarAnimalesApplication.class, args);
    }

}
